import time
from retro import retro
import cv2
from flask import Flask, Response, render_template


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


def next_frame():
    env = retro.make(game='Airstriker-Genesis')
    obs = env.reset()
    while True:

        obs, rew, done, info = env.step(env.action_space.sample())
        img = env.render(mode='rgb_array')
        success, encoded_image = cv2.imencode('.jpeg', img)
        frame = encoded_image.tobytes()
        time.sleep(0.015)
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

        if done:
            env.reset()

    env.close()

@app.route('/video_feed')
def video_feed():
    return Response(next_frame(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')
